package de.henningbrinkmann.sonocounter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.text.html.HTMLDocument;
import java.io.IOException;
import java.math.BigDecimal;

public class SonoHomepage
{

    private final Document document;

    SonoHomepage() throws IOException {
        document = Jsoup.connect("https://sonomotors.com/de/").get();
    }

    Integer getCount() {
        final Elements elements = document.select(".value-container > h1:nth-child(1)");
        final Element element = elements.first();
        if (element != null) {
            return Integer.valueOf(element.attr("data-value"));
        }

        return 0;
    }
}
