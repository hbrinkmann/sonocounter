package de.henningbrinkmann.sonocounter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SonocounterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SonocounterApplication.class, args);
	}

}
