package de.henningbrinkmann.sonocounter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.MessageFormat;

@RestController
@RequestMapping(value = "/metrics", produces = "text/plain")
@ResponseBody
public class MetricController {
    private static final Logger logger = LoggerFactory.getLogger(MetricController.class);

    @GetMapping
    public String getMetrics() {
        Integer result = 0;
        final SonoHomepage homepage;
        try {
            homepage = new SonoHomepage();
            result = homepage.getCount();
        } catch (IOException e) {
            logger.error("Failed to get count.", e);
        }

        return "sono " + result + "\n";
    }
}
