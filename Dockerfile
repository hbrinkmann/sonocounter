FROM openjdk

ADD target/sonocounter-0.0.1-SNAPSHOT.jar sono-counter.jar

EXPOSE 8080

CMD [ "java", "-jar", "sono-counter.jar" ]
